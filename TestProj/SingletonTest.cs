using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DesignPatterns.Singleton;
using Singleton;
using Xunit;

namespace TestProj
{
    public class SingletonTest
    {
        [Fact]
        private void Assert_01()
        {
            // 线程安全测试
            int[] set = new int[20];
            Task[] tList = new Task[20];
            for (int i = 0; i < 20; i++)
            {
                int j = i;
                var t = Task.Run(() =>
                {
                    set[j] = Singleton_01.GetInstance.GetHashCode();
                });
                tList[i] = t;
            }
            Task.WaitAll(tList);
            Assert.True(set.All(a => a == set[0]), "创建出了不同对象");
        }

        [Fact]
        private void Assert_02()
        {
            // 线程安全测试
            int[] set = new int[20];
            Task[] tList = new Task[20];
            for (int i = 0; i < 20; i++)
            {
                int j = i;
                var t = Task.Run(() =>
                {
                    set[j] = Singleton_02.GetInstance.GetHashCode();
                    Console.WriteLine(set[j]);
                });
                tList[i] = t;
            }
            Task.WaitAll(tList);
            Assert.True(set.All(a => a == set[0]), "创建出了不同对象");
        }

        [Fact]
        private void Assert_03()
        {
            // 线程安全测试
            int[] set = new int[20];
            Task[] tList = new Task[20];
            for (int i = 0; i < 20; i++)
            {
                int j = i;
                var t = Task.Run(() =>
                {
                    set[j] = Singleton_03.GetInstance.GetHashCode();
                });
                tList[i] = t;
            }
            Task.WaitAll(tList);
            Assert.True(set.All(a => a == set[0]), "创建出了不同对象");
        }

        [Fact]
        private void Assert_04()
        {
            // 线程安全测试
            int[] set = new int[20];
            Task[] tList = new Task[20];
            for (int i = 0; i < 20; i++)
            {
                int j = i;
                var t = Task.Run(() =>
                {
                    set[j] = Singleton_04.GetInstance.GetHashCode();
                });
                tList[i] = t;
            }
            Task.WaitAll(tList);
            Assert.True(set.All(a => a == set[0]), "创建出了不同对象");
        }

        [Fact]
        private void Assert_05()
        {
            // 线程安全测试
            int[] set = new int[20];
            Task[] tList = new Task[20];
            for (int i = 0; i < 20; i++)
            {
                int j = i;
                var t = Task.Run(() =>
                {
                    set[j] = Singleton_05.GetInstance.GetHashCode();
                });
                tList[i] = t;
            }
            Task.WaitAll(tList);
            Assert.True(set.All(a => a == set[0]), "创建出了不同对象");
        }
    }
}
