﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;
using DesignPatterns.Singleton;

namespace Singleton
{
    /// <summary>
    /// 饿汉式
    /// 使用静态构造函数加载，本质上和 <see cref="Singleton_01"/> 没有区别
    /// </summary>
    public class Singleton_02 
    {
        /// <summary>
        ///  静态字段，CLR保证线程安全
        /// </summary>
        private static readonly Singleton_02 single;

        static Singleton_02()
        {
            single = new Singleton_02();
        }

        /// <summary>
        /// 私有构造函数，保证外部无法 new 新的对象
        /// </summary>
        private Singleton_02() { }

        /// <summary>
        /// 静态属性，用于获取对象实例
        /// </summary>
        public static Singleton_02 GetInstance => single;

        #region 测试数据

        private int count = 0;

        public int Test()
        {
            return count++;
        }

        #endregion

    }
}
