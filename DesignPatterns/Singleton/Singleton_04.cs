﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Singleton
{
    /// <summary>
    /// 懒汉式，线程安全
    /// 类加载到内存后，并不实例化，在获取的时候进行判断，实例化
    /// 优点：不使用就不实例化，且<see cref="Singleton_03"/>引入了线程安全保证，但依旧有缺点 不推荐！
    /// 缺点：每次获取实例时都有一个判断,。
    /// </summary>
    public class Singleton_04
    {
        /// <summary>
        ///  静态字段，CLR保证线程安全
        /// </summary>
        private static Singleton_04 single = null;

        /// <summary>
        /// 私有构造函数，保证外部无法 new 新的对象
        /// </summary>
        private Singleton_04()
        {
        }

        /// <summary>
        /// 静态属性，获取时先做判断，为就进行实例化
        /// </summary>
        public static Singleton_04 GetInstance
        {
            get
            {
                if (single == null)
                {
                    Interlocked.CompareExchange(ref single, new Singleton_04(), null);
                }
                return single;
            }
        }

        #region 测试数据

        private int count = 0;

        public int Test()
        {
            return count++;
        }

        #endregion

    }
}
