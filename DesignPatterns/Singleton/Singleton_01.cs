﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;
using DesignPatterns.Singleton;

namespace Singleton
{
    /// <summary>
    /// 饿汉式
    /// 类加载到内存后，就实例化一个单例，CLR保证线程安全
    /// 有点：简单实用，推荐！
    /// 缺点：不管使用与否，类加载是就完成实例化（但是话说回来，如果不使用干嘛加载它）
    /// </summary>
    public class Singleton_01
    {
        /// <summary>
        ///  静态字段，CLR保证线程安全
        /// </summary>
        private static readonly Singleton_01 single = new Singleton_01();

        /// <summary>
        /// 私有构造函数，保证外部无法 new 新的对象
        /// </summary>
        private Singleton_01(){}

        /// <summary>
        /// 静态属性，用于获取对象实例
        /// </summary>
        public static Singleton_01 GetInstance => single;
        
        #region 测试数据

        private int count = 0;

        public int Test()
        {
           return count++;
        }

        #endregion

    }
}
