﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Singleton
{
    /// <summary>
    /// 懒汉式，完美方式
    /// 类加载到内存后，并不实例化，在获取的时候进行判断，实例化
    /// 优点：不使用就不实例化，线程安全保证，且<see cref="Singleton_04"/>不需要每次都判断 推荐！
    /// 缺点：每次获取实例时都有一个判断,。
    /// </summary>
    public class Singleton_05
    {
        /// <summary>
        ///  静态字段，CLR保证线程安全
        /// </summary>
        private static Singleton_05 single = null;

        /// <summary>
        /// 私有构造函数，保证外部无法 new 新的对象
        /// </summary>
        private Singleton_05()
        {
        }

        /// <summary>
        /// 静态内部类，保证了未调用就不实例化
        /// </summary>
        private class InternalSingleton
        {
            public static readonly Singleton_05 instance = new Singleton_05();
        }

        /// <summary>
        /// 静态属性，获取时先做判断，为就进行实例化
        /// </summary>
        public static Singleton_05 GetInstance => InternalSingleton.instance;

        #region 测试数据

        private int count = 0;

        public int Test()
        {
            return count++;
        }

        #endregion

    }
}
