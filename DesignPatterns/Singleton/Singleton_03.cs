﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    /// <summary>
    /// 懒汉式
    /// 类加载到内存后，并不实例化，在获取的时候进行判断，实例化
    /// 优点：不使用就不实例化，不推荐！
    /// 缺点：每次获取实例时都有一个判断,且存在线程不安全问题。
    /// </summary>
    public class Singleton_03
    {
        /// <summary>
        ///  静态字段，CLR保证线程安全
        /// </summary>
        private static Singleton_03 single;
        
        /// <summary>
        /// 私有构造函数，保证外部无法 new 新的对象
        /// </summary>
        private Singleton_03() { }

        /// <summary>
        /// 静态属性，获取时先做判断，为就进行实例化
        /// </summary>
        public static Singleton_03 GetInstance => single ??= new Singleton_03();

        #region 测试数据

        private int count = 0;

        public int Test()
        {
            return count++;
        }

        #endregion

    }
}
